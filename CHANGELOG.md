# Version.Update.Hotfix (DD-MM-YYYY)

## 1.3.9 (26-02-2025)

- Update to Unity 6
- Smaller performance changes
- Some code cleanup
- Removal of Debug code for performance reasons

## 1.3.8 (30-07-2024)

- Added "DefaultExecutionOrder" attribute to the MicroAudio script, ensuring MicroAudio initializes before other scripts.

## 1.3.7 (29-07-2024)

- Fixed event for "OnSoundFinished" where it wouldnt work properly if you played source again immediately
- Static "Update" event for MicriAudio remembered callbacks between sessions and caused errors
- If MicroAudio controller is destroyed and it is saved instance, instance is freed

## 1.3.6 (24-07-2024)

- Added contact section to the README

## 1.3.5 (04-06-2024)

- Fixed bug of throwing error when using custom sound source and changing scene (thanks to nellow_p)

## 1.3.4 (22-04-2024)

- Removed "using UnityEditor.PackageManager;" from the "DelayedSound.cs" script that results in error when building project (Visual Studio randomly adding "using")

## 1.3.3 (02-04-2024)

- Fixed problem where 'Dont Destroy On Load' didn't work properly if Game Object was nested into another

## 1.3.2 (28-03-2024)

- Fixed bug where sounds didn't pool properly, where system was unable to play sounds after few executions

## 1.3.1 (18-03-2024)

- Added channel to the debug messages
- Created sounds won't play on awake

## 1.3.0 (06-02-2024)

- Refactored lots of code and streamlined API
- Added pause/resume and stop features to sounds, music, infinity sound
- Added SFX channel to the Sounds channel
- Delay can now be paused
- Merged Music Tracks debug with Music Debug
- Updated demo scene to include pause/resume and stop features for music
- Added Infinity Sound feature to the demo scene
- Added more events for example when sound finished
- Added summary for API methods
- Fixed visual bug in demo scene for displaying which track is currently being played
- Fixed smaller bugs
- Updated documentation to reflect changes

## 1.2.0 (19-12-2023)

- Added Micro Infinity Sounds, sound effect type that allows for procedural playing of sounds.
  Looped sound effects won't sound monotonous anymore.
- Added debugging for the new Infinity Sounds
- New split reserved and delayed sound effects apart

## 1.1.1 (14-12-2023)

- Fixed problem of calling 2 events of track ending
- Fixed problem of not calling track end events if skipping track

## 1.1.0 (13-12-2023)

- Added checks and handling for empty sound clips
- Added checks and handling for empty music clips and groups
- Added checks and handling if audio manager is not present in the scene
- Fixed crossfade debugger reports
- Fixed crossfade event calls
- Fixed duplicate call of music started event when starting music groups
- Can bypass crossfade when starting music group even if crossfade is enabled
- Added debugger messages for:
  - Manager not in the scene
  - Music clip is null
  - Music group is null
  - Sound clip is null

## 1.0.0 (13-11-2023)

- First complete version
